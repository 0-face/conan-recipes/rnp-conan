
#include <rnp.h>

#include <iostream>

int main(void)
{
    std::cout<<"************ example **********" << std::endl;

    std::cout << "rnp version: " << rnp_version_string() << std::endl;

    std::cout<<"*******************************" << std::endl;

    return 0;
}

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile, tools
import os
from os import path

class TestRecipe(ConanFile):
    settings = "os", "compiler", "arch"
    build_requires = (
        "waf/0.1.1@noface/stable",
        "WafGenerator/0.2.0@noface/stable"
    )

    generators = "Waf"
    exports = "*.cpp", "wscript"

    def imports(self):
        self.copy("*.dll"   , src="bin", dst="bin")
        self.copy("*.dylib*", src="lib", dst="bin")

    def build(self):
        cmd = 'waf -v configure build -o "{}"'.format(path.join(self.build_folder, 'build'))

        self.output.info("Running cmd '{}' in '{}'".format(cmd, self.source_folder))
        self.run(cmd, cwd=self.source_folder)

    def test(self):
        if tools.cross_building(self.settings):
            self.output.info("Cross building - skip run test")
            return

        cmd = path.join(self.build_folder, 'build', 'example')

        self.output.info("running {}".format(cmd))
        self.run(cmd)

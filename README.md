# rnp - conan package

[Conan.io](https://www.conan.io/) package for [rnp][home].

>"rnp" is a set of OpenPGP (RFC4880) tools that works on
  Linux, *BSD and macOS as a replacement of GnuPG. It is
  maintained by Ribose after being forked from NetPGP,
  itself originally written for NetBSD.
>
> "librnp" is the library used by rnp for all OpenPGP functions,
  useful for developers to build against. Thanks to Alistair, it
  is a "real" library, not a wrapper like GPGME of GnuPG.
>
> NetPGP was originally written (and still maintained) by
  Alistair Crooks of NetBSD.

[home]: https://github.com/riboseinc/rnp


## How to use

See [conan docs](http://docs.conan.io/en/latest/) for instructions in how to use conan,
and look for the [library repo][repo] to more info about rnp.

[repo]: https://github.com/riboseinc/rnp

## License

This conan package is distributed under the [unlicense](http://unlicense.org/)
terms (see UNLICENSE.md).

rnp is distributed under a mix of BSD-* and Apache2 licenses,
see full license [here][license].

[license]: https://github.com/riboseinc/rnp/blob/master/LICENSE.md


#!/usr/bin/env python
# -*- coding: utf-8 -*-


from os import path

from conan.packager import ConanMultiPackager
from conans import tools

def main():
    builder = ConanMultiPackager(
                    reference=get_package_reference(),
                    build_types=["Release"],
                    build_policy="outdated")

    builder.add_common_builds(shared_option_name="rnp:shared", pure_c=False)

    filter_invalid_builds(builder)

    builder.run()

#####################################################################

def filter_invalid_builds(builder):
    filtered_builds = []
    for settings, options, env_vars, build_requires, reference in builder.items:
        if valid_settings(settings):
            filtered_builds.append([settings, options, env_vars, build_requires])

    builder.builds = filtered_builds

def valid_settings(settings):
    if not is_linux(settings):
        return True

    compiler = settings["compiler"]
    version = float(settings["compiler.version"])
    libcxx = settings["compiler.libcxx"]

    invalid = (compiler == "gcc" and version > 5 and libcxx != "libstdc++11") \
           or (compiler == 'clang' and libcxx not in ['libstdc++11', 'libc++'])

    return not invalid

def is_linux(settings):
    if "os" in settings:
        return settings["os"] != "Linux"

    return tools.os_info.is_linux

#####################################################################

def get_package_reference():
    pkg_info = get_package_info()

    return "{}/{}".format(pkg_info[0], pkg_info[1])

def get_package_info():
    import inspect

    conanfile = import_module(path.abspath('conanfile.py'))
    module_members = inspect.getmembers(conanfile, is_recipe)
    print("module_members({}): {}".format(conanfile, module_members))

    for name, member in module_members:
         if member.__module__ == conanfile.__name__:
             return (member.name, member.version)

    return None

def import_module(module_path):
    import sys
    import imp

    # Change module name to avoid conflict with ConanMultiPackager
    name = 'temp_' + path.splitext(path.basename(module_path))[0]

    sys.dont_write_bytecode = True
    loaded = imp.load_source(name, module_path)
    sys.dont_write_bytecode = False

    return loaded

def is_recipe(member):
    import inspect
    from conans import ConanFile

    return inspect.isclass(member) and issubclass(member, ConanFile)


#####################################################################

if __name__ == "__main__":
    main()

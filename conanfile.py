from conans import ConanFile, CMake, tools
from conans.model.version import Version

from os import path

class Recipe(ConanFile):
    name        = "rnp"
    version     = "0.11.0"
    description = "Implementation of OpenPGP(RFC4880)"

    license     = "BSD+Apache2 (https://github.com/riboseinc/rnp/blob/master/LICENSE.md)"
    repo_url    = "https://github.com/riboseinc/rnp"
    url         = "https://gitlab.com/0-face/conan-recipes/rnp-conan"

    settings    = "os", "compiler", "arch"
    generators  = "cmake"
    exports     = ("CMakeLists.txt",)

    options     = {
        "shared": [True, False]
    }
    default_options = (
        "shared=True"
    )

    requires = (
        "zlib/1.2.11@conan/stable",
        "bzip2/1.0.6@conan/stable",
        "json-c/0.13.1@bincrafters/stable",
        "botan/2.7.0@bincrafters/stable" # Not in conan center
    )

    SHA256 = '0f4047d965273c5b75cff1bc7019168df3c2a7f214805c83885a7b7f268c4ab6'

    def build_requirements(self):
        if not self.has_cmake_version('3.7'):
            self.output.info("Adding cmake_installer as build requirement")
            self.build_requires("cmake_installer/3.12.1@conan/stable")

    def source(self):
        url="{}/archive/v{}.tar.gz".format(self.repo_url, self.version)

        tools.get(url, sha256=self.SHA256)

        self.fix_sources()

    def build(self):
        cmake = CMake(self)
        
        self.configure_cmake(cmake)

        cmake.build()
        cmake.install()
        
    def package(self):
        self.copy("*", src=self.install_dir, keep_path=True, symlinks=True)
        self.copy("LICENSE*.md", src=self.src_dir)

    def package_info(self):
        self.cpp_info.libs        = ["rnp-0"]
        self.cpp_info.includedirs = [path.join('include', 'rnp-0')]

##################################### Properties ################################################

    @property
    def src_dir(self):
        return "rnp-" + self.version

    @property
    def src_path(self):
        return path.join(self.source_folder, self.src_dir)

    @property
    def build_path(self):
        return path.join(self.build_folder, 'build')

    @property
    def install_dir(self):
        return 'install'

    @property
    def install_path(self):
        return path.join(self.build_folder, self.install_dir)

    @property
    def lib_is_shared(self):
        return self.options.shared in [True, "True"]

####################################### Steps ################################################

    def fix_sources(self):
        self.copy_conan_cmake()
        self.patch_sources()

    def copy_conan_cmake(self):
        import shutil

        root_cmake     = path.join(self.src_path, "CMakeLists.txt")
        original_cmake = path.join(self.src_path, "CMakeListsOriginal.cmake")

        shutil.move(root_cmake, original_cmake)
        shutil.move("CMakeLists.txt", root_cmake)

    def patch_sources(self, strict=True):
        '''
        Patches:
            * Remove use of pkg-config to find json-c and Botan libs,
              because generates errors with paths.
        '''

        cmake_modules = path.join(self.src_path, 'cmake', 'Modules')
        findjson_file = path.join(cmake_modules, 'FindJSON-C.cmake')
        findbotan_file = path.join(cmake_modules, 'FindBotan2.cmake')

        tools.replace_in_file(
            findjson_file,
            'pkg_check_modules(PC_JSON-C QUIET json-c)',
            ''
            , strict=strict)

        tools.replace_in_file(
            findjson_file,
            'pkg_check_modules(PC_JSON-C QUIET json-c12)',
            ''
            , strict=strict)

        tools.replace_in_file(
            findbotan_file,
            'pkg_check_modules(PC_BOTAN2 QUIET botan-2)',
            ''
            , strict=strict)

    def configure_cmake(self, cmake):
        cmake.definitions["BUILD_TESTING"]= "OFF"
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = self.install_path
        cmake.definitions["BUILD_SHARED_LIBS"] = self.lib_is_shared

        cmake.configure(
            source_dir = self.src_path,
            build_dir  = self.build_path
        )

####################################### Helpers ################################################

    def has_cmake_version(self, version_str):
        version = self.get_cmake_version()

        if version is None or version < Version(version_str):
            if version is None:
                self.output.warn("cmake not found")
            else:
                self.output.warn("cmake version '{}' is invalid, required '{}'"
                        .format(version, version_str))

            return False

        return True

    def get_cmake_version(self):
        import subprocess
        from conans.util.files import decode_text

        if hasattr(CMake, 'get_version'):
            return CMake.get_version()

        # Adapted from conan source code
        out = None
        try:
            out, _ = subprocess.Popen(
                        ["cmake", "--version"],
                        stdout=subprocess.PIPE).communicate()
        except: # no cmake
            return None

        version_line = decode_text(out).split('\n', 1)[0]
        version_str = version_line.rsplit(' ', 1)[-1]

        return Version(version_str)
